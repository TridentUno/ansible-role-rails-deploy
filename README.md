gulfscei.rails.deploy
=========

Ansible role to configure target server specific changes.

Requirements
------------

None

Role Variables
--------------

```
# the user on the deploy target that the rails app will run under 
deploy_user: 

# the public keys to be inserted into the deploy target
ssh_public_keys:

# the deploy private key 
server_gitlab_deploy_ssh_key:
```

Dependencies
------------

None

Requirements File
-----------------

```
- src: git@gitlab.com:TridentUno/ansible-role-rails-deploy.git
  scm: git
  version: v0.1
  name: gulfscei.rails.deploy
```



Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: gulfscei.rails.deploy }

License
-------

MIT

Author Information
------------------

Contact `drward3@uno.edu` for information about this role.
